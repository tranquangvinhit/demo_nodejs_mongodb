const express = require("express");
const bcrypt = require("bcrypt");
const validResgister = require("../util.js/valid");
const User = require("../models/user");
const Brank = require("../models/brank");
const app = express();

app.post("/register", async (req, res) => {
  const { body } = req;
  if (await User.findOne({ username: body.username })) {
    return res
      .status(401)
      .json({ success: false, message: "Username is already taken" });
  }
  const { username, password, cf_password, fullname, email, phone } = body;
  const errMsg = validResgister(
    username,
    password,
    cf_password,
    fullname,
    email,
    phone
  );
  if (errMsg) return res.status(400).json({ err: errMsg });
  try {
    const listBrank = await Brank.insertMany(body.branks);
    const newUser = new User({
      username: body.username,
      email: body.email,
      fullname: body.fullname,
      password: bcrypt.hashSync(body.password, 10),
      address: body.address,
      phone: body.phone,
      branks: listBrank,
    });
    await newUser.save();
    res.send(newUser);
  } catch (error) {
    res.status(500).send(error);
  }
});
module.exports = app;
