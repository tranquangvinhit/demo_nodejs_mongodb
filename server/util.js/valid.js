const valid = (username, password, cf_password, fullname, email, phone) => {
  if (!username || !password || !fullname || !phone) {
    return "Please input all fiels!";
  }
  if (!validateEmail(email)) {
    return "Invalid emails";
  }
 
  if (password.length < 6) {
    return "Pasword must be at least 6 characters";
  }
  if (password !== cf_password) return "Confirm password did not match";
};

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}


module.exports = valid;
