const express = require("express");
const mongoose = require("mongoose");

const userRouter = require("./routes/userRoutes");
const url =
  "mongodb+srv://dev:dev123456@cluster0.remdr.mongodb.net/demo?retryWrites=true&w=majority";
const app = express();

app.use(express.json());
mongoose.connect(url, { useUnifiedTopology: true, useNewUrlParser: true });

app.use(userRouter);
app.listen(3000, () => {
  console.log("server is running");
});
