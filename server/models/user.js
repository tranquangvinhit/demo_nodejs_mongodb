const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
   
    username:{
        type :String,
        require:true
    },
    fullname:{
        type :String,
        require:true
    },
    password:{
        type :String,
        require:true
    },
    phone:{
        type :String,
        require:true
    },
    email:{
        type :String
    },
    address:{
        type :String
    },
    branks: [{ type: mongoose.Schema.Types.ObjectId, ref:'Brank' }],
})
const User = mongoose.model('User', UserSchema);
module.exports = User