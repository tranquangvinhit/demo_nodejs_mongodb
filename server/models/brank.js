const mongoose = require('mongoose');
const BrankSchema = new mongoose.Schema({
   
    brankName:{
        type: String,
        require:true
    },
    accountName:{
        type: String,
        require:true
    },
    accountNumber:{
        type: String,
        require:true
    },
})
const Brank = mongoose.model('Brank', BrankSchema);
module.exports = Brank
